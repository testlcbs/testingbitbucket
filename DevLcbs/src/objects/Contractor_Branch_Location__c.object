<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Used to store branch locations for contractors.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Contractor Branch Locations</relationshipLabel>
        <relationshipName>Contractor_Branch_Locations</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>City__c</fullName>
        <externalId>false</externalId>
        <label>City</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Country__c</fullName>
        <externalId>false</externalId>
        <label>Country</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Email__c</fullName>
        <externalId>false</externalId>
        <label>Email</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Phone__c</fullName>
        <externalId>false</externalId>
        <label>Phone</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>State__c</fullName>
        <externalId>false</externalId>
        <label>State</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Street__c</fullName>
        <externalId>false</externalId>
        <label>Street</label>
        <length>250</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Time_Zone__c</fullName>
        <externalId>false</externalId>
        <label>Time Zone</label>
        <picklist>
            <picklistValues>
                <fullName>(GMT+02:00) Eastern European Time (Africa/Cairo)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+02:00) South Africa Standard Time (Africa/Johannesburg)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+03:00) East Africa Time (Africa/Nairobi)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-09:00) Alaska Standard Time (America/Anchorage)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-05:00) Colombia Time (America/Bogota)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-03:00) Argentina Time (America/Argentina/Buenos_Aires)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-04:30) Venezuela Time (America/Caracas)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-06:00) Central Standard Time (America/Chicago)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-07:00) Mountain Standard Time (America/Denver)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-04:00) Atlantic Standard Time (America/Halifax)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-05:00) Peru Time (America/Lima)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-08:00) Pacific Standard Time (America/Los_Angeles)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-06:00) Central Standard Time (America/Mexico_City)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-05:00) Eastern Standard Time (America/New_York)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-05:00) Eastern Standard Time (America/Panama)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-07:00) Mountain Standard Time (America/Phoenix)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-04:00) Atlantic Standard Time (America/Puerto_Rico)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-02:00) Brasilia Summer Time (America/Sao_Paulo)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-03:30) Newfoundland Standard Time (America/St_Johns)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-08:00) Pacific Standard Time (America/Tijuana)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+03:00) Arabian Standard Time (Asia/Baghdad)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+07:00) Indochina Time (Asia/Bangkok)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+05:30) India Standard Time (Asia/Kolkata)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+05:30) India Standard Time (Asia/Colombo)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+06:00) Bangladesh Time (Asia/Dhaka)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+04:00) Gulf Standard Time (Asia/Dubai)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+08:00) Hong Kong Time (Asia/Hong_Kong)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+07:00) Western Indonesia Time (Asia/Jakarta)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+02:00) Israel Standard Time (Asia/Jerusalem)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+04:30) Afghanistan Time (Asia/Kabul)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+12:00) Magadan Time (Asia/Kamchatka)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+05:00) Pakistan Time (Asia/Karachi)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+05:45) Nepal Time (Asia/Kathmandu)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+08:00) Malaysia Time (Asia/Kuala_Lumpur)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+03:00) Arabian Standard Time (Asia/Kuwait)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+08:00) Philippine Time (Asia/Manila)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+06:30) Myanmar Time (Asia/Rangoon)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+03:00) Arabian Standard Time (Asia/Riyadh)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+07:00) Indochina Time (Asia/Ho_Chi_Minh)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+09:00) Korean Standard Time (Asia/Seoul)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+08:00) Singapore Standard Time (Asia/Singapore)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+08:00) Taipei Standard Time (Asia/Taipei)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+05:00) Uzbekistan Time (Asia/Tashkent)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+04:00) Georgia Time (Asia/Tbilisi)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+03:30) Iran Standard Time (Asia/Tehran)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+09:00) Japan Standard Time (Asia/Tokyo)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+05:00) Yekaterinburg Time (Asia/Yekaterinburg)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-04:00) Atlantic Standard Time (Atlantic/Bermuda)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-01:00) Cape Verde Time (Atlantic/Cape_Verde)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-02:00) South Georgia Time (Atlantic/South_Georgia)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+10:30) Australian Central Daylight Time (Australia/Adelaide)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+10:00) Australian Eastern Standard Time (Australia/Brisbane)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+09:30) Australian Central Standard Time (Australia/Darwin)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+11:00) Lord Howe Daylight Time (Australia/Lord_Howe)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+08:00) Australian Western Standard Time (Australia/Perth)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+11:00) Australian Eastern Daylight Time (Australia/Sydney)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+08:00) China Standard Time (Asia/Shanghai)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+01:00) Central European Time (Europe/Amsterdam)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+02:00) Eastern European Time (Europe/Athens)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+01:00) Central European Time (Europe/Berlin)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+01:00) Central European Time (Europe/Brussels)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+02:00) Eastern European Time (Europe/Bucharest)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+00:00) Greenwich Mean Time (Europe/Dublin)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+02:00) Eastern European Time (Europe/Helsinki)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+02:00) Eastern European Time (Europe/Istanbul)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+00:00) Western European Time (Europe/Lisbon)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+00:00) Greenwich Mean Time (Europe/London)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+03:00) Further-eastern European Time (Europe/Minsk)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+03:00) Moscow Standard Time (Europe/Moscow)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+01:00) Central European Time (Europe/Paris)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+01:00) Central European Time (Europe/Prague)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+01:00) Central European Time (Europe/Rome)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+00:00) Greenwich Mean Time (GMT)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+13:00) New Zealand Daylight Time (Pacific/Auckland)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+13:45) Chatham Daylight Time (Pacific/Chatham)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+13:00) Phoenix Islands Time (Pacific/Enderbury)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+12:00) Fiji Time (Pacific/Fiji)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+11:00) Solomon Islands Time (Pacific/Guadalcanal)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-10:00) Hawaii-Aleutian Standard Time (Pacific/Honolulu)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+14:00) Line Islands Time (Pacific/Kiritimati)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-11:00) Niue Time (Pacific/Niue)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+11:30) Norfolk Islands Time (Pacific/Norfolk)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-11:00) Samoa Standard Time (Pacific/Pago_Pago)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+13:00) Tonga Time (Pacific/Tongatapu)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-05:00) Eastern Standard Time (America/Indiana/Indianapolis)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-06:00) Central Standard Time (America/El_Salvador)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT-03:00) Chile Summer Time (America/Santiago)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>(GMT+01:00) Central European Time (Africa/Algiers)</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Zip_Postal_Code__c</fullName>
        <externalId>false</externalId>
        <label>Zip/Postal Code</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Contractor Branch Location</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Contractor Branch Location Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Contractor Branch Locations</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
