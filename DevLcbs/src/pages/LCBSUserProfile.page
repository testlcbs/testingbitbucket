<!--------------
Name         : LCBSUserProfile
Created By   : Chiranjeevi Gogulakonda
Company Name : NTT Data
Project      : LCBS
Created Date : 
Test Class   :  
Test Class URL: 
Usages       : 
Modified By  :
---------->

<apex:page controller="LCBSManageUsersController" showChat="false" showHeader="false" sidebar="false" standardStylesheets="true" >
  <style>
    @font-face{
    font-family: lcbs_font;
    src: url("{
      !URLFOR($Resource.LCBS_Font)}
  ");
}
  
  </style>
  
  <style type="text/css">
    body {
      font-family: Oxygen;
    }
    .btn-default {
      color: #333;
      background-color: #fff;
      border-color: #ccc;
    }
    .btn {
      display: inline-block;
      padding: 6px 12px;
      width:100%;
      margin-bottom: 0;
      font-size: 14px;
      font-weight: 400;
      line-height: 1.42857143;
      text-align: center;
      white-space: nowrap;
      vertical-align: middle;
      touch-action: manipulation;
      cursor: pointer;
      -webkit-user-select: none;
      background-image: none;
      border: 1px solid transparent;
    }
    a {
      color: #337ab7;
      text-decoration: none;
    }
  </style>
  
  <apex:includescript value="//code.jquery.com/jquery-1.11.1.min.js" / >
    <apex:includescript value="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" />
    <apex:stylesheet value="//cdn.datatables.net/1.10.4/css/jquery.dataTables.css" />
    <script>
      
      j$ = jQuery.noConflict();
      j$(document).ready( function () {
        j$('.dropdown-toggle').dropdown();
        var contactTable = j$('[id$="contacttable"]').DataTable({
          order: [[0, 'asc']],
          aoColumns : [
            null,
            null,
            null,
            {
              "bSortable": false }
            ,
            {
              "bSortable": false }
            ,
            {
              "bSortable": false }
            
          ],
          initComplete: function() {
            var api = this.api();
            var select = j$('[id$=accountSelect]');
            api.column(0).data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' )
            }
                                                     );
            
          }
          
        }
                                                               );
        
        //j$('[id$=accountSelect]').change(function() {
        //  var val = j$.fn.dataTable.util.escapeRegex(
        //    j$(this).val()
        //);
        //contactTable.column(0)
        //  .search( val == 'All' ? '' : '^'+val+'$', true, false )
        //  .draw();
        //});
        
      }
                        );
      
    </script>
    <apex:form >
      <html>
        <head>
          <meta charset="utf-8" />
          <meta http-equiv="X-UA-Compatible" content="IE=edge" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta name="description" content="" />
          <meta name="author" content="" />
          <title>
            Honeywell LCBS Contractor Portal
          </title>
          <!-- Bootstrap core CSS -->
          <apex:stylesheet value="{!URLFOR($Resource.LCBS_CSS, 'css/bootstrap.min.css')}" />
          <apex:stylesheet value="{!URLFOR($Resource.LCBS_CSS, 'css/honeywell-lcbs.css')}" />
          <apex:stylesheet value="{!URLFOR($Resource.LCBS_CSS, 'css/sticky-footer.css')}" />
          <apex:includeScript value="{!URLFOR($Resource.LCBS_JS, 'js/bootstrap.js')}"/>
          <apex:includeScript value="{!URLFOR($Resource.LCBS_JS, 'js/bootstrap.min.js')}"/>
          <apex:includeScript value="{!URLFOR($Resource.LCBS_JS, 'js/npm.js')}"/>
          
          <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
          <!--[if lt IE 9]>
<script src="js/ie8-responsive-file-warning.js">
</script>
<![endif]-->
          <script src="js/ie-emulation-modes-warning.js">
          </script>
          <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
          <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js">
</script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js">
</script>
<![endif]-->
        </head>
        
        <body>
    <c:LCBSCommunityHeader />
            
            
            <div style="background-color:#595959; color:grey;padding:20px; margin-top:-2px; text-align:center;">
              <h1>
                User Profile
              </h1>
            </div>
            <br/>
            <apex:pagemessages />
            
            <apex:outputPanel id="RefreshTable">
              <div class="container">
                
                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="well">
                      <p>
                        <strong>
                          First Name
                        </strong>
                        :{!contactEdit.FirstName}
                      </p>
                      
                      <p>
                        <strong>
                          Last Name
                        </strong>
                        : {!contactEdit.LastName}
                      </p>
                      
                      <p>
                        <strong>
                          Role
                        </strong>
                        :{!contactEdit.Role__c} 
                      </p>
                      
                      <p>
                        <strong>
                          Contractor Company Location
                        </strong>
                        : {!contactEdit.Contractor_Company_Location__r.name}
                      </p>
                      
                      <p>
                        <strong>
                          Phone Number
                        </strong>
                        : {!contactEdit.Phone}
                      </p>
                      
                      <p>
                        <strong>
                          Email / Username
                        </strong>
                        : 
                        <a href="#">
                          {!contactEdit.Email}
                        </a>
                      </p>
                      
                      <a class="btn btn-default" href="apex/LCBSEditUser?Id={!contactEdit.Id}" role="button">
                        Edit 
                      </a>
                      
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="well">
                      <p>
                        <strong>
                          Assigned Buildings
                        </strong>
                        :
                      </p>
                      
                      <div class="table-responsive">
                        <table class="table" id="contacttable" >
                          <tr>
                            
                            <th>
                              Building Name 
                              <span class="glyphicon glyphicon-sort">
                              </span>
                            </th>
                            <th>
                              Location
                              <span class="glyphicon glyphicon-sort">
                              </span>
                            </th>
                            <th>
                            </th>
                            
                            
                            <apex:repeat value="{!BuildList}" var="build">
                              <tr>
                                <td>
                                  {!build.name}
                                </td>
                                
                                <td>
                                  {!build.Contractor_Company_Location__r.name}
                                </td>
                                <td>
                                  <a class="btn btn-default" href="/apex/LCBS_Building_MoreInfo_BuildInfo?Id={!build.Id}" role="button">
                                    Edit
                                  </a>
                                  
                                </td>
                                
                              </tr>
                              
                            </apex:repeat>
                            
                            
                            <!-- 
<tr>

<td>
{!BuildList[1].name}
</td>
<td>
{!BuildList[1].Contractor_Company_Location__r.name}
</td>
<td>
<a class="btn btn-default" href="/apex/LCBSBuildingOwnerEdit" role="button">
Edit
</a>
</td>

</tr>

<tr>
<td>
{!BuildList[2].name}
</td>
<td>
{!BuildList[2].Contractor_Company_Location__r.name}
</td>
<td>
<a class="btn btn-default" href="/apex/LCBSBuildingOwnerEdit" role="button">
Edit 
</a>

</td>
</tr>
-->
  </tr>
  
  </table>
  </div>
  
  
  </div>
              </div>
              </div>
              
            </div>
            </apex:outputPanel>
            <!-- /.container -->
            <!-- Bootstrap core JavaScript
================================================== -->
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js">
            </script>
            
            <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
            <script src="js/ie10-viewport-bug-workaround.js">
            </script>
            
        </body>
      </html>
      
    </apex:form>
    
  </apex:page>