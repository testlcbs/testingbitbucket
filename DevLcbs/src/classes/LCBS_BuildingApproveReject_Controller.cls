public without sharing class LCBS_BuildingApproveReject_Controller 
{
       public Building__c fetchinfo { get; set; }
       public Account fetchinfoacc{get;set;}
       public Building_Owner__c owner {get;set;}     
       public String currentRecordId {get;set;}
       public String currentParam {get;set;}
       public string executive_name {get;set;}
       public string executive_email {get;set;}
       public string executive_phone {get;set;}
       public transient List<Messaging.SingleEmailMessage> sitemails1 {get;set;}
       public transient List<Messaging.SingleEmailMessage> sitemails {get;set;}
       public Contact con{get;set;}
      
       public LCBS_BuildingApproveReject_Controller() 
       {  

            fetchinfo = new Building__c();
            currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
            currentParam = ApexPages.CurrentPage().getparameters().get('var');
            
          
            fetchinfo = [select id,name,Owner_Approved__c,Owner_Rejected__c,EULA_Agreement__c,Contractor_Company_Location__r.Company_Name__c,Contractor_Company_Location__c,Building_Owner__c,Technician__c,Building_Owner__r.name,Building_Owner__r.Email_Address__c,Building_Owner__r.Phone_Number__c,Contractor_owner__c,Contractor_owner__r.name,Contractor_owner__r.phone,Contractor_owner__r.email,ContractorCompany__r.id  from Building__c where id =: currentRecordId ];
          //  owner = [select id,name,First_Name__c,Last_Name__c,Email_Address__c from Building_Owner__c where id=:fetchinfo.Building_Owner__c]; 
          
            con = [select id,name,phone,Account.Name,account.Account_Exec_Name__c,account.Account_Exec_Email__c,account.Account_Exec_Phone__c,LCBS_Program_Principal__c,role__c from Contact where LCBS_Program_Principal__c = true and Role__c='Owner' and Accountid=:fetchinfo.ContractorCompany__r.id];         
           system.debug(con);
          //  fetchinfoacc =[select id, email__c from account where id =:fetchinfo.Contractor_Company_Location__r.Company_Name__c];
      //      BuildName=fetchinfo.name;
            system.debug('*****fetchinfo '+fetchinfo); 
          /*    if(ac.Account_Executive_Info__c != NULL)
             
             { 
            String[] exec = ac.Account_Executive_Info__c .split('\\|');
             executive_name =exec[0];
             executive_email =exec[1];
             executive_phone =exec[2];
             }
             
             else
             {
              executive_name ='';
              executive_email ='';
              executive_phone ='';
             } */
           
       }
       
        public PageReference buildApprove() {
        system.debug('Current Parameter:'+currentParam);
        if(currentParam == 'Accept'){
        if(fetchinfo.EULA_Agreement__c== False || fetchinfo.Owner_Approved__c == False ){
        approveAction();
        }
        }
        if(currentParam == 'Decline'){
            if( fetchinfo.Owner_Rejected__c== False){
             rejected();
            }
        }
        
        
        return null;
    }

       
       public boolean Approved {
       get {
           if(fetchinfo.Owner_Approved__c == true){
              
               return true;
               }
              else
              {
               
                return false;
                }
         
       }
       set;
       }
     public boolean rejected {
       get {
           if(fetchinfo.Owner_Rejected__c == true){
              
               return true;
               }
              else
              {
               
                return false;
                }
         
       }
       set;
       }
        
        public void approveAction()
        {
          
            try{

            system.debug('*****fetchinfo '+fetchinfo); 
            fetchinfo.EULA_Agreement__c=True;
            fetchinfo.Owner_Approved__c=True;
            fetchinfo.Owner_Approved_date__c=system.now();
            update fetchinfo;            
            
            //newly added
            
          
           List<String> sendTo = new List<String>();
            //sendTo.add(fetchinfo.Contractor_Company_Location_del__r.Email_Address__c);
             Contact c = [select id, Email from Contact where email != NULL limit 1];

            sendTo.add(fetchinfo.Building_Owner__r.Email_Address__c);
            sitemails = new List<Messaging.SingleEmailMessage>();          
            Messaging.SingleEmailMessage sitemail = new Messaging.SingleEmailMessage();
           // sendTo.add(owner.Email_Address__c);
            sitemail.setToAddresses(sendTo);
            sitemail.setSenderDisplayName('Honeywell LCBS Connect');
            //sitemail.setSubject('Building Access Confirmed');            
            //string emailMessage ='Building : '+ ' '+fetchinfo.Name+' has been approved ';
            //sitemail.setHtmlBody(emailMessage);
            EmailTemplate myEMailTemplate = [Select id,name from EmailTemplate where Name ='LCBS_BuildingApproved']; 
            sitemail.setTemplateId(myEMailTemplate.id);
            sitemail.WhatId = fetchinfo.id;
            sitemail.setTargetObjectId(con.id);
            sitemails.add(sitemail);
                      
              Savepoint sp = Database.setSavepoint();
           
            if(sitemails.size()>0){
            Messaging.sendEmail(sitemails);
            Database.rollback(sp);
            
             List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
             for (Messaging.SingleEmailMessage email : sitemails) {
             system.debug('inside the for loop');
             Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
             emailToSend.setToAddresses(email.getToAddresses());
             emailToSend.setPlainTextBody(email.getPlainTextBody());
             emailToSend.setHTMLBody(email.getHTMLBody());
             emailToSend.setSubject(email.getSubject());
             lstMsgsToSend.add(emailToSend);
            }
            Messaging.sendEmail(lstMsgsToSend);      
            Task task = new Task();
            task.WhatId = fetchinfo.id;
            task.WhoId = con.id;
            task.Subject = 'Building Access accepted';
            task.status = 'Completed';
            task.Description ='TIME :   '+system.now() + '\r\n'+ '\r\n' + 'APPROVER DETAILS' + '\r\n' + 'NAME :  ' + fetchinfo.Building_Owner__r.name +'\r\n'+ 'EMAIL :  ' + fetchinfo.Building_Owner__r.Email_Address__c + '\r\n' + ' PHONE NUMBER :   ' + fetchinfo.Building_Owner__r.Phone_Number__c;
            task.ActivityDate = Date.today();
            insert task;     
         
           
            system.debug('*****fetchinfo '+fetchinfo); 
            
       //     ApprovalMessage = 'Building Approved Successsfully';
            }
            
            }
            catch (DMLException ex){
            
            system.debug(ex.getMessage());
         //   ApprovalMessage = 'ERROR: Unable to Approve Building. Contact Administrator';
            
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Building record does not exists');
            ApexPages.addMessage(msg);
            
            Approved = false;
             } 
          // PageReference p = new PageReference('/apex/LCBS_BuildingApproveReject?Id='+currentRecordId);
           //p.setRedirect(true); 
          //  return p; 
        }
      public void rejected()
      {
      displayPopup = false;
       fetchinfo.Owner_Rejected__c=True;
       update fetchinfo;
        Rejected= false;
       
     /*  try{
            system.debug('*****fetchinfo '+fetchinfo); 
            fetchinfo.Owner_Rejected__c=True;
            update fetchinfo;               
            // newly added
             List<String> sendTo = new List<String>();
            List<String> sendToCC = new List<String>();
            LCBS_NotificationMailIds__c cs = LCBS_NotificationMailIds__c.getInstance('devlcbs');            
            sendTo.add(fetchinfo.Building_Owner__r.Email_Address__c);
            sendToCC.add(cs.HMA_Email_Address1__c);  
            sendToCC.add(cs.HMA_Email_Address2__c);  
            list<Messaging.SingleEmailMessage> sitemails = new List<Messaging.SingleEmailMessage>();        
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(sendToCC);
            mail.setCcAddresses(sendTo);
            mail.setSenderDisplayName('Building Approval Status');
            mail.setSubject('Building Rejected');            
           // string emailMessage ='Building : '+ ' '+fetchinfo.Name+' has been Rejected ';
              String emailMessage = '<font face="Consolas"><br/>'
             +' Access Request Rejected for '+fetchinfo.Name+' <br/> <br/>'  
             + 'Contractor Details:<br/><br/>'
             +'Contractor Name -'+ fetchinfo.Contractor_owner__r.name +'<br/>'
             +'Contractor Email -'+ fetchinfo.Contractor_owner__r.Email+'<br/>'
             +'Contractor Phone -'+ fetchinfo.Contractor_owner__r.Phone +'<br/><br/>'          
             +'Building Details:<br/><br/>'          
             +'Building Owner Name -'+ fetchinfo.Building_Owner__r.name +'<br/>'
             +'Building Owner Phone-'+ fetchinfo.Building_Owner__r.Phone_Number__c+'<br/>'
             +'Building Owner Email-'+ fetchinfo.Building_Owner__r.Email_Address__c+'<br/>';
             mail.setHtmlBody(emailMessage);
             sitemails.add(mail);
             Messaging.sendEmail(sitemails);               
            
            Task task = new Task();
            task.WhatId = fetchinfo.id;
            task.WhoId = fetchinfo.Technician__c;
            task.Subject = 'Building Access Rejected';
            task.status = 'Completed';
            task.Description ='TIME :   '+system.now() + '\r\n'+ '\r\n' + 'APPROVER DETAILS' + '\r\n' + 'NAME :  ' + fetchinfo.Building_Owner__r.name +'\r\n'+ 'EMAIL :  ' + fetchinfo.Building_Owner__r.Email_Address__c + '\r\n' + ' PHONE NUMBER :   ' + fetchinfo.Building_Owner__r.Phone_Number__c;
            task.ActivityDate = Date.today();
            insert task;             
            system.debug('*****fetchinfo '+fetchinfo); 
            
       //     ApprovalMessage = 'Building Approved Successsfully';
            }
            
            
            catch (DMLException ex){
            
            system.debug(ex.getMessage());
         //   ApprovalMessage = 'ERROR: Unable to Approve Building. Contact Administrator';
            
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Building record does not exists');
            ApexPages.addMessage(msg);
            
            Approved = false;
             } */
          // PageReference p = new PageReference('/apex/LCBS_BuildingApproveReject?Id='+currentRecordId);
           //p.setRedirect(true); 
          //  return p; 
        } 
        public boolean displayPopup {get; set;}
        public void showPopup(){
        displayPopup = true;
    }
    public void closePopup(){
        displayPopup = false;
    }
         
}