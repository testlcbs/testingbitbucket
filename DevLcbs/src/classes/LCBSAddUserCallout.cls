Public class LCBSAddUserCallout{

  //  @future(callout=true)
    // public static integer Count= 1;

    public static void onAfterEulaAccept(String ContactList){ 
        string ContactId = ContactList;
        
        String fedid;
        LCBS_RequestResponse_Log__c logs = new LCBS_RequestResponse_Log__c();
        List<Contact> lstCon = [SELECT ID,FirstName,Name,LastName,Email,Phone,Role__c,Account.RecordType.Name,Account.EnvironmentalAccountType__c,LCBS_UserName__c,LCBS_Password__c,LCBS_FedId__c FROM Contact where id=:ContactList];
        Set<Id> vSetId = new Set<Id>();
        for(Contact vCon: lstCon){
            vSetId.add(vCon.id);
        }
        
        azureactiveDirectoryHelper activeDirectoryHelper = new azureactiveDirectoryHelper();
        String authorizationHeader = activeDirectoryHelper.getLcbsWebApiAccessToken();
        system.debug('Access Token:'+authorizationHeader );
        Http http=new Http();
        HttpRequest req =new HttpRequest();
        HTTPResponse res = new HTTPResponse();      
        JSONGenerator gen = JSON.createGenerator(true);
        
        for(Contact c2 :lstCon){                  
            gen.writeStartObject();        
            gen.writeObjectField('FirstName',c2.FirstName); 
            gen.writeObjectField('MiddleName','');
            gen.writeObjectField('LastName',c2.LastName); 
            if(c2.Account.RecordType.Name == 'ECC Americas ShipTo - Trade')
            {
            gen.writeObjectField('UserType','Distributor');              
            }
            if(c2.Account.RecordType.Name == 'Contractors')
            {
            gen.writeObjectField('UserType','Contractor');              
            }
            
            gen.writeObjectField('Email',c2.Email); 
            //gen.writeObjectField('PrimaryPhoneNumber',c2.Phone); 
            gen.writeObjectField('PrimaryPhoneNumber','9293178535');
            gen.writeObjectField('AlternatePhoneNumber','');
            gen.writeObjectField('Country','US');
           // gen.writeFieldName('Role');
          //  gen.writeStartObject(); 
           // gen.writeObjectField('RoleName',c2.Role__c);          
          //  gen.writeEndObject();  
            gen.writeEndObject();                         
        }
        String jsonString = gen.getAsString();
        system.debug('jsonString>>>>'+jsonString);
        
        try{
        req.setHeader('Authorization', authorizationHeader); 
        req.setHeader('Content-Type', 'application/json');
          
        List<LCBS_API_Details__c> CalloutDetails= LCBS_API_Details__c.getall().values();
        
        if (CalloutDetails[1].AddUserAPIDetails__c !=null && CalloutDetails[1].AddUserAPIDetails__c !='') {
          req.setEndpoint(CalloutDetails[1].AddUserAPIDetails__c);
          system.debug('req.setEndPoint+++'+CalloutDetails[1].AddUserAPIDetails__c);
        }
        req.setMethod('POST');
        req.setCompressed(false);
        req.setTimeout(120000);
        req.setBody(jsonString);
        res= http.send(req);    
        String addUserResponse = res.getBody();
        system.debug('Azure Response++'+res);
        system.debug('Azure Response body++'+addUserResponse);
        system.debug('Status of Request++'+res.getStatusCode());    
        JSONParser parser = JSON.createParser(addUserResponse);
        LCBS_APILogs.captureLogs(jsonString,res.getBody(),res.getStatusCode());
        string userName;
        string  Azurepassword;
        string  AzureFedId;
        while (parser.nextToken()!= null) {
        //system.debug('cccccccc000022 parser.getCurrentToken(  '+parser.getCurrentToken());
        if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
        String fieldName = parser.getText();
        //system.debug('cccccccc000022 fieldName '+fieldName);
        parser.nextToken();
       
        if(fieldName == 'userName') {
            userName = parser.getText();
            system.debug('cccccccc000022 userName '+userName);
        }
        if(fieldName == 'password') {
             Azurepassword = parser.getText();
            system.debug('cccccccc000022 Azurepassword '+Azurepassword);
        }
        if(fieldName == 'federationIdentifier') {
             AzureFedId = parser.getText();
            system.debug('cccccccc000022 AzureFedId'+AzureFedId);
        }
        }
       }
        //system.debug('1cccccccc000022 userName '+userName);
        //system.debug('1cccccccc000022 Azurepassword '+Azurepassword);
        //system.debug('1cccccccc000022 AzureFedId'+AzureFedId);
        lstCon[0].LCBS_UserName__c = userName;
        lstCon[0].LCBS_Password__c = Azurepassword;
        lstCon[0].LCBS_FedId__c  = AzureFedId;
        
        update lstCon;
        String ContId = lstCon[0].id;
        system.debug('Contact Id*****:'+ ContId );
        CreatePortalUser.PortalUserInfo(ContId,AzureFedId,userName);  
        }
         catch(Exception ex)
        {
            system.debug(ex.getMessage());
            LCBS_APILogs.captureLogs(jsonString,res.getBody(),res.getStatusCode());
        }
        /*catch(System.CalloutException ex)
        {
        system.debug(ex.getMessage());
        Integer StatusCode = res.getStatusCode();
        logs.Name= 'Add User API Exception';
        logs.Request__c = jsonString;
        logs.Response__c = res.getBody();
        if(StatusCode ==202)
        logs.Status__c = 'Success';
        else
        //res= http.send(req); 
         if(Count<5){
         Count++;
        LCBSAddUserCallout.onAfterEulaAccept(ContactId );
        }
        else
        {logs.Status__c = 'Failed';
        insert logs;}
            
        }
        */
             
    }
}