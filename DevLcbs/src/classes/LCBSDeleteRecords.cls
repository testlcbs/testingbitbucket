public class LCBSDeleteRecords Implements Database.Batchable<sObject>{

    public final String Query;
    public final String s_Object;
    
    public LCBSDeleteRecords(String q, String s){
        Query = q;
        s_Object = s;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> batch){
        List <sObject> lstsObject = new list<sObject>();
        for(sObject b : batch)
        {
            sObject a = (sObject)b;
            lstsObject.add(a);
        }
        Delete lstsObject;
    }
    
    public void finish(Database.BatchableContext BC){
        //Send an email to the User after your batch completes
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'bharat.gunreddy@nttdata.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Apex Batch Job is done');
        mail.setPlainTextBody('The batch Apex job processed');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

}

//This is how the batch class is called.
//Using Developer Console.
/*
String q = 'SELECT Id FROM Account LIMIT 20';
String s = 'Account';
ID batchinstanceid = database.executeBatch(new LCBSDeleteRecords(q, s));
*/