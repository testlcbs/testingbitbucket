@isTest
public with sharing class LCBS_BuildingOwnerTrigger_Test{
    public static testmethod void BuildingOwnerTriggerTest() {
         Building_Owner__c bo = new  Building_Owner__c(Name = 'Test Building Owner');
         insert bo;
         update bo;
         delete bo;
         undelete bo;
    }
}