public without sharing class LCBS_BuildingOwnerEmail_Controller {
public Id companyNames {get;set;}
public string buildingOwnerName{get;set;}
public string orgid {get;set;}
public document logo {get;set;}
public document sign {get;set;}
public string sfdcBaseURL{get;set;}
public Id buildId{get;set;}
public String d {get;set;}
 
 public LCBS_BuildingOwnerEmail_Controller()
 {
  //con = [select id,name,phone from Contact where AccountId =:companyNames and Role__c='Owner' limit 1];
  d= DateTime.now().format('MMMMM dd, yyyy');
  sfdcBaseURL = System.URL.getSalesforceBaseURL().toExternalForm();
  logo = [select id,name from document where name='LCBS_Email_Logo'];
  sign = [select id,name from document where name='Honeywell vicepresident sign']; 
  orgid= UserInfo.getOrganizationId();
   
  sign = [select id,name from document where name='Honeywell vicepresident sign']; 
 }
  public Building__c build {
        get {
            build = [select id,name,CreatedById,Building_Owner__r.Name,Building_Owner__r.email_address__c from building__c where id =:buildId];
            system.debug('fetchinfo<<<<<<<<:'+build);
            return build;                                                
        } 
        set;
    } 
    public User u {
        get {
           u = [select id,name,AccountId from user where id=: build.createdByid];
           return u;                                                
        } 
        set;
    } 
    public Contact con {
        get {
          con = [select id,name,phone,Account.name,Account.Email__c from Contact where AccountId=:u.AccountId and Role__c='Owner' and LCBS_Program_Principal__c =true limit 1];
           return con;                                                
        } 
        set;
    }    
}