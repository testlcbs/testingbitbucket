Public Class CreatePortalUser{
//@future
    public static void PortalUserInfo(String ContactId,String FederationId,string AzureUsername){
        
        Contact con = [select id,email,firstName,lastname,accountId from Contact where Id =:contactId];
        //Profile profileId= [SELECT Id,name FROM Profile where name = 'Custom Customer Community Login User'];         
        
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.EmailHeader.triggerUserEmail = false;       
        dmo.EmailHeader.triggerOtherEmail = false;
        dmo.EmailHeader.triggerAutoResponseEmail = false;       
        dmo.optAllOrNone = false;
        
        // create portal user
       // string nick = con.email!=null?con.email.substring(0, con.email.indexOf('@')):'';
        string alia;
        string nick;
        if(((con.lastname).length())>=4){
            alia=(((con.FIRSTNAME).substring(0,1))+((con.LASTNAME).substring(0,4)));
            nick = (((con.FIRSTNAME).substring(0,1))+((con.LASTNAME).substring(0,4))); 
        }else{
            integer len=(con.lastname).length();
            alia=(((con.FIRSTNAME).substring(0,1))+((con.LASTNAME).substring(0,len)));
            nick = (((con.FIRSTNAME).substring(0,1))+((con.LASTNAME).substring(0,len)));
        }
        nick += Datetime.now().getTime();
        User newUser = new User(
            //alias = createAlias(con.firstName, con.lastName), 
            alias = alia,
            email = con.email, 
            emailencodingkey = 'UTF-8', 
            firstname = con.firstName, 
            lastname = con.lastname, 
            languagelocalekey = 'en_US', 
            localesidkey = 'en_US', 
            contactId = con.Id,
            timezonesidkey = 'Asia/Dubai', 
            //username = con.email+'.ntttest123457890123112',
            username = AzureUsername,
            CommunityNickname = nick,
            ProfileId = label.CustomProfileID ,
            FederationIdentifier=FederationId,
            IsActive = true);
        
        newUser.setOptions(dmo);
        insert newUser;
        contact c = [select id,iscontainfedid__c from contact where id=:contactId];
        c.iscontainFedId__c = true;
        update c;
    }
}