public class LCBSBuildingCallout
{
 
 @future(callout = true)
 public static void getBuildings(string BuildingName,string BuildingId, string BuildingOwnerId, string BuildingOwnerName)
 {
    User u1;
    User u2;
    User u3;
    User u4;
    azureactiveDirectoryHelper activeDirectoryHelper = new azureactiveDirectoryHelper();
    String authorizationHeader = activeDirectoryHelper.getLcbsWebApiAccessToken();
    
    HttpRequest req = new HttpRequest();
    HttpResponse res = new HttpResponse();
    Http http = new Http();
    Building__c build = new Building__c();
    build = [select createdBy.Contact.Account.id,createdBy.Name,createdBy.id,createdBy.FederationIdentifier, Dispatcher__c,Technician__c,Alternate_Dispatcher__c,Alternate_Technician__c,Dispatcher__r.id,Technician__r.id,Alternate_Dispatcher__r.id,Alternate_Technician__r.id from Building__c where id=:BuildingId];
    if(build.Dispatcher__c!= NULL) 
    {
    u1= [select id,name,Contact.ContactType__c,FederationIdentifier from user where contact.id=:build.Dispatcher__r.id];
    }
    if(build.Technician__c!= NULL)
    {
    u2= [select id,name,Contact.ContactType__c,FederationIdentifier from user where contact.id=:build.Technician__r.id];
    }
    if(build.Alternate_Dispatcher__c!= NULL)
    {
    u3= [select id,name,Contact.ContactType__c,FederationIdentifier from user where contact.id=:build.Alternate_Dispatcher__r.id];
    }
    if(build.Alternate_Technician__c != NULL) 
    {
    u4= [select id,name,Contact.ContactType__c,FederationIdentifier from user where contact.id=:build.Alternate_Technician__r.id];
    }
    JSONGenerator gen = JSON.createGenerator(true);
    gen.writeStartObject();        
    gen.writeObjectField('BuildingName', BuildingName); 
    gen.writeObjectField('BuildingId', BuildingId); 
    gen.writeFieldName('ContractorCompanyUsers');
    gen.writeStartArray();
    
    gen.writeStartObject();
    gen.writeObjectField('ExternalAppUserId',build.createdBy.id);
    gen.writeObjectField('Name',build.createdBy.name);
    gen.writeObjectField('UserType','ContractorCompanyOwner');
    gen.writeObjectField('UserId', build.createdBy.FederationIdentifier);
    gen.writeEndObject();
  
   /* if(build.Dispatcher__c!= NULL) 
    {
    gen.writeStartObject();
    gen.writeObjectField('ExternalAppUserId',u1.id);
    gen.writeObjectField('Name',u1.name);
    gen.writeObjectField('UserType','PrimaryDispatcher');
    gen.writeObjectField('UserId',u1.FederationIdentifier);
    gen.writeEndObject();
    }
    
    if(build.Technician__c!= NULL)
    {
    gen.writeStartObject();
    gen.writeObjectField('ExternalAppUserId',u2.id);
    gen.writeObjectField('Name',u2.name);
    gen.writeObjectField('UserType','PrimaryTechnician');
    gen.writeObjectField('UserId',u2.FederationIdentifier);
    gen.writeEndObject();
    }
    
    if(build.Alternate_Dispatcher__c!= NULL)
    {
    gen.writeStartObject();
    gen.writeObjectField('ExternalAppUserId',u3.id);
    gen.writeObjectField('Name',u3.name);
    gen.writeObjectField('UserType','SecondaryDispatcher');
    gen.writeObjectField('UserId',u3.FederationIdentifier);
    gen.writeEndObject();
    }
    
    if(build.Alternate_Technician__c != NULL)
    {
    gen.writeStartObject();
    gen.writeObjectField('ExternalAppUserId',u4.id);
    gen.writeObjectField('Name',u4.name);
    gen.writeObjectField('UserType','SecondaryTechnician');
    gen.writeObjectField('UserId',u4.FederationIdentifier);
    gen.writeEndObject();
    } */
    
    gen.writeEndArray();
    
    gen.writeFieldName('BuildingOwner');
    gen.writeStartObject(); 
    gen.writeObjectField('ExternalAppUserId',BuildingOwnerId);
    gen.writeObjectField('Name',BuildingOwnerName);
    gen.writeEndObject();
    gen.writeEndObject();
    String jsonString = gen.getAsString();

    system.debug('jsonString +++'+jsonString );
    //string addBuildingJsonRequest = JSON.serialize(B);
    system.debug('Authorization Header +++'+ authorizationHeader);
    req.setHeader('Authorization', authorizationHeader); 
    req.setHeader('Content-Type', 'application/json');
    List<LCBS_API_Details__c> CalloutDetails= LCBS_API_Details__c.getall().values();
    
    if (CalloutDetails[0].End_Pt_URL__c!=null && CalloutDetails[0].End_Pt_URL__c!='') {
      req.setEndpoint(CalloutDetails[0].End_Pt_URL__c+build.createdBy.Contact.Account.id+'/buildings');
    }
    
    system.debug('req.setEndPoint+++'+CalloutDetails[0].End_Pt_URL__c+build.createdBy.Contact.Account.id+'/buildings');
    req.setMethod('POST');
    req.setCompressed(false);
    req.setBody(jsonString);
    
    res= http.send(req);
    String buildingResponse = res.getBody();
    system.debug(buildingResponse);
    system.debug('Status of Request++'+res.getStatusCode());
    
  }   
 
 public class Jsonserialize
{
    string BuildingName;
    String BuildingId;
    String BuildingOwnerId;
    String BuildingOwnerName;     
}
 }