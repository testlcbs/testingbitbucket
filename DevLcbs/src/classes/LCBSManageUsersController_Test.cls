@isTest(SeeAllData=false)
public class LCBSManageUsersController_Test
{
     
    static Contact contact;
    static Company_Location__c comploc;
    static Building__c crtbll ;
   
    static testMethod void myTestManageUser(){
       
        contact=LCBS_TestDataFactory.createContact(3)[0];
        comploc=LCBS_TestDataFactory.createcompanyLocation(1)[0];
        crtbll = LCBS_TestDataFactory.createBuilding(1)[0];
        
        
        PageReference pageRef = Page.LCBSManageUsers;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('id',contact.id);
        Id conId =contact.id;
                 
        LCBSManageUsersController manageusr = new LCBSManageUsersController();
     
    
            //BEGIN: some setup items
        Profile randomProfile = [SELECT Id FROM Profile WHERE UserType = 'CspLitePortal' LIMIT 1];
        //create accounts for our portal users
        List<account> partnerAccounts = new List<account>();
        partnerAccounts.add(new Account(Name = 'Testing Communities Company'));
        insert partnerAccounts;
        //Create some Contacts because we want to create portal users we are required to provide a corresponding contactId
        List<contact> partnerContacts = new List<contact>();
        partnerContacts.add(new Contact(AccountId = partnerAccounts[0].Id, Email = 'communitiesupdateduser@interactiveties.com', FirstName = 'Demo1', LastName = 'User1'));
        insert partnerContacts;
        List<user> newUsers = new List<user>();
        newUsers.add(new User(Alias = 'test01', ContactId = partnerContacts[0].Id, Email = 'communitiesupdateduser@interactiveties.com', EmailEncodingKey = 'ISO-8859-1', FirstName = 'Demo1', IsActive = true, LanguageLocaleKey = 'en_US', LastName = 'User1', LocaleSidKey = 'en_US', MobilePhone = '(303) 555-0000', Phone = '(303) 555-2222', ProfileId = randomProfile.Id, TimeZoneSidKey = 'America/New_York', Username = 'communitiesupdateduser@interactiveties.com'));
        insert newUsers;
        //END: some setup items
        //validate the initial contact information
        Contact c = [SELECT Email, FirstName, LastName, MailingCity, MailingCountry, MailingPostalCode, MailingState, MailingStreet, Phone, Title FROM Contact WHERE Id =: partnerContacts[0].Id];
        System.assertEquals('communitiesupdateduser@interactiveties.com', c.Email);
        System.assertEquals('Demo1', c.FirstName);
        System.assertEquals('User1', c.LastName);
        System.assertEquals(null, c.MailingCity);
        System.assertEquals(null, c.MailingCountry);
        System.assertEquals(null, c.MailingPostalCode);
        System.assertEquals(null, c.MailingState);
        System.assertEquals(null, c.MailingStreet);
        System.assertEquals(null, c.Phone);
        System.assertEquals(null, c.Title);
        //perform the test
        Test.startTest();
        System.runAs ( new User(Id = newUsers[0].Id) ) { //runAs in order to avoid MIXED_DML_OPERATION error
            List<user> userUpdates = new List<user>();
            userUpdates.add(new User(City = 'San Francisco', Country = 'US', FirstName = 'Tess', Id = newUsers[0].Id, LastName = 'Hacic', PostalCode = '94105', State = 'CA', Street = 'One Market Street', Title = 'CEO'));
            update userUpdates;
        }
        Test.stopTest();
        //validate that it worked as intended
        c = [SELECT Email, FirstName, LastName, MailingCity, MailingCountry, MailingPostalCode, MailingState, MailingStreet, Phone, Title FROM Contact WHERE Id =: partnerContacts[0].Id];
        System.assertEquals('communitiesupdateduser@interactiveties.com', c.Email);
        System.assertEquals(null, c.MailingCity);
        System.assertEquals(null, c.MailingCountry);
        System.assertEquals(null, c.MailingPostalCode);
        System.assertEquals(null, c.MailingState);
        System.assertEquals(null, c.MailingStreet);
        System.assertEquals(null, c.Title);
        
        
        manageusr.outboundEmail();    
         
        manageusr.updateUser();
        manageusr.cancelUser();
        
       // system.runas(admin){
       // manageusr.DeactivateUser();
     //   }
        manageusr.newUser(); 
        manageusr.getTimeZoneOptions();
        delete manageusr.ContactList;
        
        
        List<SelectOption> testGetRole = manageusr.getRoleoptions();
        List<SelectOption> testComLoc = manageusr.getCompanyLocOptions();
        
        
    }
}