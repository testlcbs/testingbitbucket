public with sharing class LCBS_Distributor_CompanyInfo_Controller {


 public String tabOpt { get; set; }
    
    public Contact fetchUserinfo { get; set; }
    public Account acct {get; set;}
    public Contact Cont{get;set;} 
    public Account AccountInfo{get; set;}   
    Public User user{get;set;} 
    public Company_Location__c contractor {get;set;}
    public string accountId{get;set;}
    public List<Contact> con{get;set;}
    public String ContactId {get;set;}
    public Contact co {get;set;}
    Public boolean CompanyEditPanel {get;set;} 
    Public boolean AddUserPanel {get;set;} 
    Public boolean EditUserPanel{get;set;}
    Public boolean EditPanel {get;set;}
    //public Boolean DepRole {set;get;}
    //document doc{get;set;}
    //Public String Filename{get;set;}                   
   // Public Blob Filebody{get;set;}
    
    public LCBS_Distributor_CompanyInfo_Controller (){

        AccountInfo= new Account();  
        
        fetchUserinfo = new Contact(); 
        con = new List<Contact>();
        
        List<Account> acc = new List<Account>();
       
         
             
          user =[select id,Contactid,name,Contact.id,Account.Id,Contact.FirstName,Contact.LastName from User where id=:userinfo.getuserId()]; 
          Account a = [select id,(select id,firstName,lastname,Contractor_Company_Location__c,Role__c,Email,Phone from contacts order by lastname) from Account where id=:user.Account.Id];
          contractor = new Company_Location__c(Company_Name__c = user.AccountId); 
          //contact = [select id,firstName,lastname,Contractor_Company_Location__c,Role__c,Email,Phone from Contact where id=:a.contacts];  
          con.addall(a.contacts);    
          try{
         // acct = [Select id,name,phone,email__c,State__c,Country__c,OwnerId from Account where Id =: user.AccountId];   
          acct = [Select id,name,phone,email__c,Address__c,State__c,Country__c,shippingStreet,shippingCity,shippingCountry,shippingState,shippingPostalCode,Time_Zone__c,(Select Name,City__c,Email_Address__c,Phone__c,Address__c,State__c,Time_Zone__c,Postal_Code__c,Owner__c,Owner_Last_Name__c from Company_Locations__r),(select FirstName,LastName from Contacts),OwnerId from Account where Id =: user.AccountId];      
          Cont = [select id,FirstName,LastName,Email,Phone,Role__c,MailingStreet,MailingCity,MailingState,MailingCountry,Time_Zone__c from Contact where id=:user.Contactid];
          contractor =[select id,Address__c,name,City__c,Country__c,Company_Name__c,Email_Address__c,Owner__c,Phone__c,State__c,Postal_Code__c,Time_Zone__c from Company_Location__c where id=:user.AccountId];
            
         //  SystemadminId = [select id from user where profile = ''];
             
          }
          
          
          catch(exception e){}
               
                system.debug('###ACCOUNTS'+acct);
                system.debug('###User ID'+userinfo.getuserId());
                system.debug('###User List '+Cont.FirstName);   
                system.debug('role*****'+Cont.Role__c);
                
           if(Cont.Role__c == 'Owner' || Cont.Role__c == 'Deputy' ){                
           CompanyEditPanel = true;  
           AddUserPanel = true; 
           EditUserPanel = true;
           EditPanel = true;
          }
          
          if(Cont.Role__c == 'User' || Cont.Role__c == 'Occupant' ){
           CompanyEditPanel = false;  
           AddUserPanel =false;  
           EditUserPanel = false; 
           EditPanel = false;   
          }
    
}
public PageReference cancelupdateUser() {
      //return null;
      string url = system.label.LCBS_Navigation_Menu_Domain ;
      PageReference pr = new PageReference(url+'LCBS_CompanyInfo#users');
      return pr;
    }


    public PageReference updateUser() {
       try{
       
       /* if( Cont.Role__c == 'Deputy' ){
            if(co.Role__c == 'Occupant'){
            system.debug('@@@@@@@@'+co.Role__c );
               DepRole = true;     
               system.debug('@@@@@@@@'+DepRole);          
               return null;
            } else{DepRole = false;}
        }*/
        update co;
        system.debug('@@@@@@@@afterUPdate>>>'+co.id);
       }
       catch(Exception e)
       {
        ApexPages.addMessages(e); 
       }
      // string url = system.label.LCBS_Navigation_Menu_Domain ;
      //PageReference pr = new PageReference(url+'LCBS_CompanyInfo');
      //return pr;
    return apexpages.currentpage(); 
        
    }


    public PageReference canceladdUser() {
       //return null;
        string url = system.label.LCBS_Navigation_Menu_Domain ;
        PageReference pr = new PageReference(url+'LCBS_CompanyInfo');
        return pr;
    }


    public PageReference addUser() {
        try{
      
        fetchUserinfo.AccountId =  user.Account.Id;
        fetchUserinfo.is_Invited__c = true ;
        fetchUserinfo.OwnerId = '005g0000001vgHSAAY';
        //fetchUserinfo.role__c =selectedRole;
        system.debug('**********chiran1111111****'+user.Account.Id);
        insert fetchUserinfo;
        
        // Logic is to send the email after user is saved 
        
        Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
            String[] sendingTo = new String[]{fetchUserinfo.Email};
            semail.setToAddresses(sendingTo);
            semail.setSubject('Invitation to Join the Web Portal');
            
           // semail.setPlainTextBody('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor and vitality, so that labor and obesity. Over the years, I will come, who shall exercise, school district nostrud aliquip advantage from it, but for to work. Duis has been criticized in the pleasure of the pain in the desire to be one irure flee cillum dolore eu produces no resultant. Excepteur cupidatat blacks are not hopefully you abandon the effeminate and their spirits, that is, my toil, sunt in culpa qui officia');
            semail.setHtmlBody('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor and vitality, so that labor and obesity. Over the years, I will come, who shall exercise, school district nostrud aliquip advantage from it, but for to work. Duis has been criticized in the pleasure of the pain in the desire to be one irure flee cillum dolore eu produces no resultant. Excepteur cupidatat blacks are not hopefully you abandon the effeminate and their spirits, that is, my toil, sunt in culpa qui officia'+
    'To view your case <a href=https://na1.salesforce.com/'+case.Id+'>click here.</a>');


            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail});
                    
        
        
        
        system.debug('**********chiran****'+fetchUserinfo);
       }
       catch(Exception e)
       {
        ApexPages.addMessages(e); 
       }
        string url = system.label.LCBS_Navigation_Menu_Domain ;
        PageReference pr = new PageReference(url+'LCBS_CompanyInfo');
        return pr;
      }

 public SelectOption[] getCompanyOwners() 
    { 
        
            SelectOption[] getCompanyNames= new SelectOption[]{};  
                getCompanyNames.add(new SelectOption('','--None--'));
            for (Contact a : [select id,Name from contact limit 10]) 
            {  
                getCompanyNames.add(new SelectOption(a.id,a.name)); 
            }
            return getCompanyNames; 
        
    }  
    public void getContactInfo()
    {
     system.debug('Test:::::::::'+ContactId);
     co = [select FirstName,LastName,Email,Phone,Role__c,MailingStreet,MailingCity,MailingState,MailingCountry,Time_Zone__c from contact where id=:ContactId];
     
    }
    public Document document {
    get {
      if (document == null)
        document = new Document();
      return document;
    }
    set;
  }
    public PageReference upload() {
 
    document.AuthorId = UserInfo.getUserId();
    document.FolderId = '00lj0000000y9f4AAA'; // put it in running user's folder
 
    try {
     document.name = user.name+'logo';
     document.IsPublic = True;
      insert document;
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
      return null;
    } finally {
      document.body = null; // clears the viewstate
      document = new Document();
    }
 
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'File uploaded successfully'));
    return null;
  }
    public PageReference updateCompany() {
   system.debug('Entering Method');
     // att = new Attachment(); document
     // att.parentId = user.Account.Id;
    //  att.name=String.Valueof(Filename);
     // att.body = Filebody;
    // document.name =  'Honeywell Logo';
   //  document.AuthorId = UserInfo.getUserId();
   //  document.FolderId = UserInfo.getUserId();
   //  document.IsPublic = true;
     

        try{
        if(cont.LastName != Null){
        //if( att.name== null || att.body == null){
        system.debug('Test Update'+cont.LastName);
        Update acct;
        update cont;
       // insert document;
        system.debug('Test Update'+acct.Phone);
       // }
        }   
        }
        catch(DmlException e){
            String error = e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
        }
       
       // return null;
          string url = system.label.LCBS_Navigation_Menu_Domain ;
          PageReference pr = new PageReference(url+'LCBS_CompanyInfo');
          return pr;
    }
    
    public PageReference cancelCompany() {
        string url = system.label.LCBS_Navigation_Menu_Domain ;
        PageReference pr = new PageReference(url+'LCBS_CompanyInfo');
        return pr;
    }
     public PageReference updateCompanyLocation() {
 
        try{
        //contractor.Company_Name__c = user.AccountId;
        insert contractor;
       
        }
        catch(DmlException e){
            String error = e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
        }
        //return null;
          string url = system.label.LCBS_Navigation_Menu_Domain ;
          PageReference pr = new PageReference(url+'LCBS_CompanyInfo');
          return pr;
    }
   // public string selectedRole{get;set;}
    public SelectOption[] getRole() 
    {     
     SelectOption[] getRoleNames= new SelectOption[]{};    
      
      if(Cont.Role__c == 'Owner' || Cont.Role__c == 'User' || Cont.Role__c == 'Occupant'  ){                
        getRoleNames.add(new SelectOption('User','User')); 
        getRoleNames.add(new SelectOption('Owner','Owner'));  
        getRoleNames.add(new SelectOption('Deputy','Deputy')); 
        getRoleNames.add(new SelectOption('Occupant','Occupant')); 
        }  
        if(Cont.Role__c == 'Deputy' ){                
        getRoleNames.add(new SelectOption('User','User'));  
        getRoleNames.add(new SelectOption('Deputy','Deputy')); 
        getRoleNames.add(new SelectOption('Occupant','Occupant')); 
        }  
                 
       /* Schema.DescribeFieldResult fieldResult =Contact.Role__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
       for( Schema.PicklistEntry f : ple)
       {
          getRoleNames.add(new SelectOption(f.getLabel(), f.getValue()));
       }     
          */                  
        return getRoleNames;         
    }
    
    public SelectOption[] getTimeZone() 
    {       
        SelectOption[] getTimeZones = new SelectOption[]{};        
        getTimeZones.add(new SelectOption('','--None--'));          
         Schema.DescribeFieldResult fieldResult =Contact.Time_Zone__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
       for( Schema.PicklistEntry g : ple)
       {
          getTimeZones.add(new SelectOption(g.getLabel(), g.getValue()));
       }                            
        return getTimeZones;         
    }
    
    public String getFileId() {
        String fileId = '';
        List<Document> attachedFiles = [select name,AuthorId from Document where name = 'Honeywell-logo1'];
        if( attachedFiles != null && attachedFiles.size() > 0 ) {
            fileId = attachedFiles[0].Id;
        }
        return fileId;    
    }
    

}