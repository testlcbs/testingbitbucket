public without sharing class Test_ContractorInviteStatusController {
    
    public Id InvContactId {get; set;}
    public String InvEmailId {get; set;}
    public Id InvAccountId {get; set;}
    public boolean ContractorInvited {
        get{
            List<Contact> c = [select id, Contractor_Invited__c from Contact where id=:InvContactId limit 1];
            return c[0].Contractor_Invited__c;
        }
    }
}