public class JSON2ApexList {

    public class Email {
        public String UserId;
        public String ReceiverEmailId;
        public String Subject;
        public String MessageBody;
        public String CreatedDate;
        public String AlertCode;
        public String Priority;
        public String BuildingId;
        public String CloudAlertId;
        
    }

    public class AlertNotificationRequest {
        public List<Email> Email;
    }

    public AlertNotificationRequest AlertNotificationRequest;

    
    public static JSON2Apex parse(String json) {
        return (JSON2Apex) System.JSON.deserialize(json, JSON2Apex.class);
    }

    /*    
    static testMethod void testParse() {
        String json = '{'+
        ' \"AlertNotificationRequest\": { '+
        '   \"Email\": {'+
        '     \"Priority\":\"High\",'+
        '     \"Body\":\"Test\",'+
        '     \"Subject\":\"Test\",'+
        '     \"TechnicianId\":\"Test\"'+
        '    }'+
        ' }'+
        '}'+
        '}';
        JSON2Apex obj = parse(json);
        System.assert(obj != null);
    }
    */
}