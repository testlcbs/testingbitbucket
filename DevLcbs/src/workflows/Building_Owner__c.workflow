<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <tasks>
        <fullName>Email_Alert_for_Activity_History</fullName>
        <assignedTo>nagarajan.varadarajan@nttdata.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Sent Email to Building Owner</subject>
    </tasks>
</Workflow>
