// Created by: Chiranjeevi Gogulakonda
 // Created Date: 10 Aug 2015. 

trigger CreateCommunityContact on User (Before Insert,after insert,before update,after update) {  
  if (trigger.isAfter) {
            if(trigger.IsInsert || Trigger.IsUpdate ){
      For(User u: Trigger.new){
          if(u.IsPortalEnabled == true){
            Contact c=[select id,Account.EnvironmentalAccountType__c from contact where id=:u.Contactid];
            if(c.Account.EnvironmentalAccountType__c == 'Contractor')
            {       
            if(!system.isfuture())
            {
            LCBSAddCompanyCallout.onAfterInsertFuture(trigger.new[0].id);    
            }
            else
            {
            LCBS_ContractorInviteDelegate_scheduler AddCompanyAPISchedule = New LCBS_ContractorInviteDelegate_scheduler(trigger.new[0].id);
            DateTime curDT = DateTime.newInstance(DateTime.now().year(), DateTime.now().month(), DateTime.now().day(), DateTime.now().Hour(), DateTime.now().minute(), DateTime.now().second());
            DateTime jobDT = curDT.addSeconds(5);
            System.debug('Job DateTime:::'+jobDT.format('MM/dd/yyyy HH:mm:ss'));
            Integer year = jobDT.year();
            Integer month = jobDT.Month();
            Integer day = jobDT.day();
            Integer hour = jobDT.hour();
            Integer min = jobDT.minute();
            Integer sec = jobDT.second();
            String sch = sec+' '+min+' '+hour+' '+day+' '+month+' ? '+year;
            System.debug('Cron EXP:::'+sch);
            String JobTime = sec+'-'+min+'-'+hour+'-'+day+'-'+month+'-'+year;
            system.schedule('Add Company - One Time Job_'+JobTime ,sch,AddCompanyAPISchedule); 
            System.debug('Test Started');
            }
            }
            }        
            }     
       }
    }
}