public with sharing class selectoptions {

 public selectoption[] myop{set;get;}

 public selectoptions(){
 
     selectoption op1= new selectoption('one', 'jan'); 
     selectoption op2= new selectoption('two', 'feb'); 
     selectoption op3= new selectoption('null','-none');  
     myop= new selectoption[]{op1,op2,op3};
    
 }

}