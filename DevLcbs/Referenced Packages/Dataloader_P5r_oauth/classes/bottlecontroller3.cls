public with sharing class bottlecontroller3 {

    public string bname{set;get;}
    public string brnd{set;get;}
    public decimal cst{set;get;}
    public decimal qty{set;get;}
    
    bottle__c bt;
    
 public bottlecontroller3(){
    bt = [select id,name,brand__c,cost__c,quantity__c from bottle__c where name='beer' limit 1];
    bname = bt.name;
    brnd = bt.brand__c; 
    cst =  bt.cost__c ;
    qty =  bt.quantity__c; 
     }
     
 public  List<Bottle__C> getBottles(){
     List<Bottle__C> bt = [select id,name,cost__c,brand__C,quantity__c from Bottle__C];
     return bt;
}
   
   }