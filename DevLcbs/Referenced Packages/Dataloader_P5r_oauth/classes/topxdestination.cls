public class topxdestination
{
       
   public static void main(list<Top_X_designation__c> li)
     {
             List<opportunity__c> listoppUpdate = new List<opportunity__c>();
             List<opportunity__c> listopp = new List<opportunity__c>();
             
             Set<Id> setOppId=new Set<Id>();
             Set<Id> setOppDelete=new Set<Id>();
             map<id,id> mapDocAttchTrue = new map<id,id>();
             map<id,id> mapDocAttchFalse = new map<id,id>();
             map<id,id> mapDelete = new map<id,id>();


            for(Top_X_Designation__c td:li)
            {
            if(td.Document_Attached__c == True && td.Type__c == 'Contract Flow Down')
            {
            mapDocAttchTrue.put(td.opportunity__c,td.id);
            setOppId.add(td.opportunity__c);
            }
            else
            mapDocAttchFalse.put(td.opportunity__c,td.id);
            setOppId.add(td.opportunity__c);
            }


            for(Top_X_Designation__c td:li)
            {
            mapDelete.put(td.opportunity__c,td.id);
            setOppId.add(td.opportunity__c);
            setOppDelete.add(td.opportunity__c);
            }

            listopp = [select id,Handoff_Attached__c from opportunity__c where id in: setOppId];
            if(listopp.size()>0 && listopp !=null)
            {
            for(opportunity__c opp:listopp){
            if(mapDocAttchTrue.containskey(opp.id))
            {
            opp.Handoff_Attached__c='Yes';
            }
            if(mapDocAttchFalse.containskey(opp.id))
            {
            opp.Handoff_Attached__c='No';
            }
            if(setOppDelete.contains(opp.id))
            {
            opp.Handoff_Attached__c='';
            }
            listoppUpdate.add(opp);
            }
            }
            if(listoppUpdate.size()>0 && listoppUpdate!=null)
            {
            update listoppUpdate;
            system.debug('LLLLLLLLLLLLLLL'+listoppUpdate);
            }

}
}