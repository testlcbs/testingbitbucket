public with sharing class mergerecords {
    
    
   String searchText;
     List<Lead__c> results;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String s) {
        searchText = s;
    }

    public List<Lead__c> getResults() {
        return results;
    }

    public PageReference doSearch() {
      
        results = Database.query('SELECT id,name FROM Lead__c');
        //system.debug('tttttt'... +results);
        
        System.debug('----------' +results);
        results = (List<Lead__c>)[FIND :searchtext RETURNING Lead__c(id,Firstname__c)][0];
        return null;
    }

}