public with sharing class theController {


     //start of search logic
      String searchText;
     
      List<Lead__c> results;

    public String getSearchText() {
        return searchText;
    }
    

    public void setSearchText(String s) {
        searchText = s;
    }

    public List<Lead__c> getResults() {
        return results;
    }

    public PageReference doSearch() {
        results = [FIND :searchText RETURNING Lead__c(id,name,Firstname__c)][0];
        return null;
    }

}
//end of search logic