global class book{

   Webservice static void insertBook(String n,String a,integer c,integer q){
     Bottle__c bt = new Bottle__c();
     bt.name = n;
     bt.brand__c = a;
     bt.cost__C = c;
     bt.quantity__c=q;
     insert bt;
  }
 }