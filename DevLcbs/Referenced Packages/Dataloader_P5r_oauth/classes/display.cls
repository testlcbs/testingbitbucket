public with sharing class display {

//To store the picklist value which we have selected on VF page i.e (Ind, Aus, USA)
    public String country { get; set; }    
    
    //To display list of picklist values on VF page
    public List<selectOption> getPicklistvalues() {
        List<selectOption> options = new List<selectOption>();       
        options.add(new selectOption('India','India'));
        options.add(new selectOption('Australia','Australia'));
        options.add(new selectOption('USA','USA'));
        return options;
    }   

    //To make three pageblocksections invisible by default
    public Boolean indtf = false;
    public Boolean austf = false;
    public Boolean usatf = false;
       
    //To dynamically pass Boolean values to rendered attribute on pageblocksection
    public void setInd(Boolean b) {
        this.indtf = b;
    }
    public Boolean getInd() {
        return this.indtf;
    }    
    
    
    public void setAus(Boolean b) {
        this.austf = b;
    }
    public Boolean getAus() {
        return this.austf;
    }

    
    public void setUsa(Boolean b) {
        this.usatf = b;
    }
    public Boolean getUsa() {
        return this.usatf;
    }
 
    //Constructor, After loading the page to display india pageblocksection by default
    public display() {
        setInd(True);
        setAus(False);
        setUsa(False);
    }
    
    //After changing picklist value based on the selection to display either usa or aus pageblocksection
    //Through actionfunction or actionsupport this method will be called to VF page
    public PageReference selectcountry() {
        if(country == 'Australia') {
            setInd(False);
            setAus(True);
            setUsa(False);
        }
        else {
            setInd(False);
            setAus(False);
            setUsa(True);
        }
        return null;
    }
}