public with sharing class ddd {


public List<SelectOption> customList
{
   get
   {
      List<SelectOption> retVal = new List<SelectOption>();

      retVal.add(new SelectOption('value1', 'Label 1'));
      retVal.add(new SelectOption('value2', 'Label 2'));
      retVal.add(new SelectOption('value3', 'Label 3'));
      // ...            

      return retVal;
   }
   private set;
}



}