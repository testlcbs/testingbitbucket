public with sharing class bottlecontroller {

    public string bname{set;get;}
    public string brnd{set;get;}
    public decimal cst{set;get;}
    public integer qty{set;get;}
    
    bottle__c bt = new bottle__c();{
    bt.name = bname;
    bt.brand__c = brnd;
    bt.cost__c = cst; 
    bt.quantity__c = qty;
    }
   
  public PageReference save(){
     bottle__c bt = new bottle__c();{
     bt.name = bname;
     bt.brand__c = brnd;
     bt.cost__c = cst; 
     bt.quantity__c = qty;
     insert bt;
     PageReference pr=new pageReference('https://ap1.salesforce.com/a00/o');
     return pr;
    }
   }
    public PageReference cancel(){
     
    
    return null;
   }
  
}