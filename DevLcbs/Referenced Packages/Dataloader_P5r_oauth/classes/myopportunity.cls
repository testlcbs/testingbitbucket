public with sharing class myopportunity {

     public boolean active { get; set;}
     public string opname{set;get;}
     public string campn{set;get;}
     public string pone{set;get;}
     public integer amnt{set;get;}
     public String typ{get;set;}
     public String ldsource{get;set;} 
     public date closedt{get;set;}
     
     
     public List<SelectOption> gettype(){
      List<SelectOption> options = new List<SelectOption>();
      options.add(new SelectOption('None','--None--'));
      options.add(new SelectOption('existingcustemer-upgrade','existingcustemer-upgrade'));
      options.add(new SelectOption('existingcustemer-downgrade','existingcustemer-downgrade'));
      options.add(new SelectOption('existingcustemer-replacmenet','existingcustemer-replacement'));
      options.add(new SelectOption('new custmer','new custemer'));
      return options; 
     }
     
      public List<SelectOption> getledsurce(){
      List<SelectOption> options = new List<SelectOption>();
      options.add(new SelectOption('None','--None--'));
      options.add(new SelectOption('web','web'));
      options.add(new SelectOption('phone enquiry','phone enquiry'));
      options.add(new SelectOption('patner referel','patner referel'));
      options.add(new SelectOption('other','other'));
      return options; 
     }
      opportunity__c opt;
      public opportunity__c getProxyObject() { 
      opt = new opportunity__c();
      return opt;
     }
     
     public PageReference check(){
            
              System.debug('----------' +typ);
              System.debug('----------' +ldsource);
             
              
              return null;
     }   
  
     public PageReference save_new(){
          opportunity__c op = new opportunity__c();
          
            op.Opportunity_Name__c = opname;
            op.Amount__c = amnt; 
            op.Lead_Source__c = ldsource;
            op.Account_Name__c=typ; 
            op.Close_Date__c = opt.Close_Date__c;
            op.Private__c= active;

            insert op;
  PageReference pr=new pageReference('https://c.ap1.visual.force.com/apex/opportunity');
            return pr;
    }
    
    public PageReference save(){
          opportunity__c op = new opportunity__c();
          
            op.Opportunity_Name__c = opname;
            op.Amount__c = amnt; 
            op.Lead_Source__c = ldsource;
            op.Account_Name__c=typ; 
            op.Close_Date__c = opt.Close_Date__c;
            op.Private__c= active;

            insert op;
  PageReference pr=new pageReference('https://ap1.salesforce.com/a0D/o');
            return pr;
    }
      public PageReference cancel(){      
    return null;
    }
    
    
    
           
}