public class BATestIW implements Database.stateful,Database.Batchable<bottle__c>{
 integer num = 10;
 public iterable<bottle__c> start(database.batchablecontext bc){
  list<bottle__c> bt = [select id,name,cost__c from bottle__c];
  return bt;
 }
 
                                             
  public void execute(Database.BatchableContext bc, LIST<Bottle__c> bt) {
  
  for(bottle__c bl:bt){
   bl.cost__c += num;  
    }
  update bt;
  system.debug('value of num'+num);
  }
  public void finish(Database.BatchableContext bc){
  system.debug('value of num'+num);
  num +=20;
  
  }

}