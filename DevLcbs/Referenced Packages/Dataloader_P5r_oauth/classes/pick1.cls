public class pick1 {

     public string nmbr{set;get;}
     
     public string fname{set;get;}
     
     public string lname{set;get;}
     
     public string comp{set;get;}
     
     public string campn{set;get;}
     
     public string pone{set;get;}
     
     public string eml{set;get;}
     
     public String rating{get;set;}
     
     public String lsource{get;set;}
     
     public String lstatus{get;set;}
     
     public date sdate{get;set;}
     
     //public lookup lp{set;get;} 
     

     public lead__c ld{
       get{
         if(ld == null)
         ld = new Lead__c();
         return ld;
       }
       set;
     }
     
     //Lookup field for Campaign   
     public Campaign__c mycamp{
       get{
         if(mycamp == null)
         mycamp = new Campaign__c(); 
         return mycamp;
       }
       set;
     }
         
     
     String searchText;
     
     //start of search logic

     List<Lead__c> results;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String s) {
        searchText = s;
    }

    public List<Lead__c> getResults() {
        return results;
    }

    public PageReference doSearch() {
        results = (List<Lead__c>)[FIND :searchtext RETURNING Lead__c(id,Firstname__c)][0];
        return null;
    }
//end of search logic
                                                                                                                                                                                                                                                                                                                                                       
   public List<SelectOption> getratings(){
   
      List<SelectOption> options = new List<SelectOption>();
      
      options.add(new SelectOption('None','--None--'));
      
      options.add(new SelectOption('Online','Online'));
      
      options.add(new SelectOption('Offline','Offline'));
      
      options.add(new SelectOption('Ideal','Ideal'));
      
      options.add(new SelectOption('Busy','Busy'));
      
      return options; 
     }
     
   public List<SelectOption> getleadsource(){
      List<SelectOption> options = new List<SelectOption>();
      options.add(new SelectOption('None','--None--'));
      options.add(new SelectOption('agriculture','agriculture'));
      options.add(new SelectOption('bio tech','bio tech'));
      options.add(new SelectOption('govt','govt'));
      options.add(new SelectOption('food','food'));
      return options; 
     } 
     
   public List<SelectOption> getleadstatus(){
      List<SelectOption> options = new List<SelectOption>();
      options.add(new SelectOption('None','--None--'));
      options.add(new SelectOption('open-notcontacted','open-notcontacted'));
      options.add(new SelectOption('working-contacted','working-contacted'));
      options.add(new SelectOption('closed-converted','closed-converted'));
      options.add(new SelectOption('closed-notconverted','closed-notconverted'));
      return options; 
     } 
   public PageReference save(){
           // lead__c ld = new lead__c();
          //  ld.Campaign__c = campn;
            insert mycamp;
            ld.Firstname__c = fname;
            ld.Lastname__c = lname;
            ld.Company__c  = comp;
            ld.Campaign__c = campn;
            ld.phone__c  =  pone;
            ld.Email__c = eml;
            ld.Rating__c = rating;
            ld.Lead_soure__c = lsource; 
            ld.Campaign__c = mycamp.name;
            ld.Leas_status__c  =lstatus; 
          
            ld.Start_Date__c = led.Start_Date__c;

            insert ld;
  PageReference pr=new pageReference('https://ap1.salesforce.com/a0C/o');
            return pr;
           }
            public PageReference save_new(){
               insert mycamp;
          //  lead__c ld = new lead__c();
         //   ld.Campaign__c = campn;
            
            ld.Firstname__c = fname;
            ld.Lastname__c = lname;
            ld.Company__c  = comp;
            ld.Campaign__c = campn;
            ld.phone__c  =  pone;
            ld.Email__c = eml;
            ld.Rating__c = rating;
            ld.Lead_soure__c = lsource; 
            ld.Leas_status__c  =lstatus; 
            ld.Campaign__c=led1.Campaign__c;
            ld.Start_Date__c = led.Start_Date__c;

            insert ld;
  PageReference pr=new pageReference('https://c.ap1.visual.force.com/apex/lead');
            return pr;
      }
  public PageReference cancel(){
    return null;
      }   
       lead__c led;
       public lead__c getproxyObject() { 
          led = new lead__c();
          return led;
       }
       
       lead__c led1;
       public lead__c getproxyObject2() { 
          led1 = new lead__c();
          return led1;
       }
       
       
       
       
              
        public String getld() {
      return null;
  }
  
  public pick1()
  {
 // instantiate your dummy object
    ld = new lead__c();
  }
  
  
  
  
     
  public PageReference check(){
            
              System.debug('----------' +rating);
              System.debug('----------' +lsource);
            //  System.debug('----------' +lstatus);
              
              return null;
           }  
           
       public PageReference delet(){
           // lead__c ld = new lead__c();
          //  ld.Campaign__c = campn;
            insert mycamp;
            ld.Firstname__c = fname;
            ld.Lastname__c = lname;
            ld.Company__c  = comp;
            ld.Campaign__c = campn;
            ld.phone__c  =  pone;
            ld.Email__c = eml;
            ld.Rating__c = rating;
            ld.Lead_soure__c = lsource; 
            ld.Campaign__c = mycamp.id;
            ld.Leas_status__c  =lstatus; 
          
            ld.Start_Date__c = led.Start_Date__c;

            delete ld;
  PageReference pr=new pageReference('https://ap1.salesforce.com/a0C/o');
            return pr;     
           
     }      
 }