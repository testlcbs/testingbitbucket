public with sharing class recordprocess {
 
 public list<account> ac{set;}
 
 public list<account> getac(){
 
  list<account> ac1=[SELECT AccountNumber,AnnualRevenue,BillingCity,Name FROM Account];
 
 return ac1;
 }
 public account ac2{set;get;}
 
 public void edit(){
 
 string x = apexpages.currentpage().getparameters().get('id');
 
  ac2 = [SELECT AccountNumber,AnnualRevenue,BillingCity,Name FROM Account where id=:x];
  
 }
  public void save(){
  
   update ac2;
   }
}