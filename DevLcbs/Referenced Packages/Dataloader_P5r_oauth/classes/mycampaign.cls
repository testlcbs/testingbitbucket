public with sharing class mycampaign {

    public string campname{set;get;}
    public integer exptrev{set;get;}
    public integer actlcost{set;get;}
    public integer budcost{set;get;}
    public integer expresp{get;set;}
    public String sts{get;set;} 
    public String typ{get;set;} 
    public date endt{get;set;}    
    public date stdt{get;set;}
    public boolean active {get;set;}

      public List<SelectOption> gettype(){
      List<SelectOption> options = new List<SelectOption>();
      options.add(new SelectOption('None','--None--'));
      options.add(new SelectOption('webinar','webinar'));
      options.add(new SelectOption('public relation','public relation`'));
      options.add(new SelectOption('confrence','confrence'));
      options.add(new SelectOption('trade show','trade show'));
      options.add(new SelectOption('patners','patners'));
      return options; 
      }
      
      public List<SelectOption> getstats(){
      List<SelectOption> options = new List<SelectOption>();
      options.add(new SelectOption('None','--45874--'));
      options.add(new SelectOption('completed','completed'));
      options.add(new SelectOption('inprogress','inprogress`'));
      options.add(new SelectOption('aborted','aborted'));
      return options; 
      }
      
      campaign__c cmp;
      public campaign__c getProxyObject() { 
      cmp = new campaign__c();
      return cmp;    
      }
      public PageReference check(){
            
              System.debug('----------' +typ);
              System.debug('----------' +sts);
              
              return null;
      }
      public PageReference save(){
          campaign__c cp = new campaign__c();
          cp.Campaign_Name__c = campname;
          cp.Active__c = active; 
          cp.Expected_Revenue__c = exptrev;
          cp.Budgeted_Cos__c = budcost; 
          cp.Actual_Cost__c = actlcost;
          cp.Expected_Response__c= expresp;
          cp.Start_Date__c = cmp.Start_Date__c;
          cp.End_Date__c = cmp.End_Date__c;
          cp.Type__c = typ;
          cp.Status__c = sts;
            
            
            insert cp;
  PageReference pr=new pageReference('https://ap1.salesforce.com/a0A/o');
            return pr;
    }
    public PageReference cancel(){
    return null;
   }
    public PageReference save_new(){
      campaign__c cp = new campaign__c();
          
           cp.Campaign_Name__c = campname;
           cp.Active__c = active; 
           cp.Expected_Revenue__c = exptrev;
           cp.Budgeted_Cos__c = budcost; 
           cp.Actual_Cost__c = actlcost;
           cp.Expected_Response__c= expresp;
           cp.Start_Date__c = cmp.Start_Date__c;
           cp.End_Date__c = cmp.End_Date__c;
           cp.Type__c = typ;
           cp.Status__c = sts;
            
            insert cp;
    PageReference pr=new pageReference('https://c.ap1.visual.force.com/apex/campaigns');
            return pr;
    
    }          
}