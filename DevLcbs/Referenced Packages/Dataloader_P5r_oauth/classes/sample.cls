public with sharing class sample {

    public sample(ApexPages.StandardController controller) {

    }

    public List<account> mem {get;set;}
    public Integer count {get;set;}
    public List<account> memList = new List<account>();
   
    public Integer index = 5;
    public Integer start = 0;
   
    public Boolean nextBool {get;set;}
    public Boolean prevBool {get;set;}
   
    public sample()
    {
        memList = [SELECT Name,AccountNumber,AnnualRevenue FROM account ORDER BY Name];
        count = [SELECT Count() FROM account];
        List<account> temp = new List<account>();
        for(Integer i = start; i<index; i++)
        {       
            temp.add(memList.get(i));
        }
        mem = temp;
        prevBool = true;
        nextBool = false;
    }
   
    public void next()
    {
        index = index + 5;
        start = start + 5;
        mem.clear();
        if(index > count)
        {
            index = Math.Mod(count,5) + start;
            system.debug('Index is ' + index);
            nextBool = true;
            prevBool = false;
            List<account> temp = new List<account>();
            for(Integer i = start; i<index; i++)
            {       
                temp.add(memList.get(i));
            }
            mem = temp;        
            index = start + 5;   
        }
        else
        {
            List<account> temp = new List<account>();
            for(Integer i = start; i<index; i++)
            {       
                temp.add(memList.get(i));
            }
            mem = temp;    
            prevBool = false;
        }  
    }
   
    public void previous()
    {
        if(start > 5)
        {   
            index = index - 5;
            start = start - 5;
            List<account> temp = new List<account>();
            for(Integer i = start; i<index; i++)
            {       
                temp.add(memList.get(i));
            }
            mem = temp;
            prevBool = false;
            nextBool = false;
        }   
        else
        {
            index = index - 5;
            start = start - 5;
            List<account> temp = new List<account>();
            for(Integer i = start; i<index; i++)
            {       
                temp.add(memList.get(i));
            }
            mem = temp;
            prevBool = true;
            nextBool = false;       
        }  
    }



}