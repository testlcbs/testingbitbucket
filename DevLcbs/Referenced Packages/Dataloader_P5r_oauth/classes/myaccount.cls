public with sharing class myaccount {
        public string acname{set;get;}
        
                public string acn{set;get;} 
                public string pacname{set;get;}
                public integer acnmbr{set;get;}
                public integer anlrev{set;get;}
                public string pne{set;get;}
                public string emal{set;get;}
                public string bstreet{set;get;}
                public string bcty{set;get;}
                public string bstatp{set;get;}
                public integer pcod{set;get;}
                public string cuntry{set;get;}
                public string shstret{set;get;}
                public string shcty{set;get;}
                public string shstatp{set;get;}
                public string shcuntry{set;get;}
                public string typ{set;get;}
                public string rting{set;get;}
                public string wship{set;get;}
              
                  public account__c ac{
                   get{
                     if(ac == null)
                     ac = new account__c();
                     return ac;
                   }
                   set;
                 }
                 
               public account__c myact{
                   get{
                     if(myact == null)
                     myact = new account__c(); 
                     return myact;
                   }
                   set;
                 }    
         
  public PageReference save(){
                       account__c ac = new account__c();
                       insert myact;
                       ac.Account_Name__c = acname;
                       ac.Account_Number__c = acnmbr;
                       ac.Annual_Revenue__c = anlrev;
                       ac.phone__c = pne; 
                       ac.Email__c = emal;
                       ac.Billing_Stree__c = bstreet;
                       ac.Billing_City__c = bcty;
                       ac.Billing_State_Province__c= bstatp;
                       ac.Billing_Zip_Postal_Code__c = pcod; 
                       ac.Billing_Country__c = cuntry;   
                       ac.Shipping_Street__c = shstret; 
                       ac.Shipping_City__c = shcty; 
                       ac.Shipping_State_Province__c= shstatp;
                       ac.Shipping_Country__c = shcuntry;
                       ac.Rating__c = rting;
                       ac.Type__c = typ;
                       ac.Ownership__c = wship;
                      // ac.account__c = ac1.account__c;
                        ac.account__c = myact.id;
                       insert ac;
                       
                      PageReference pr=new pageReference('https://ap1.salesforce.com/a0B/o');
                                return pr;
                       }
   
  public list<selectoption> gettypp(){
   
                      list<selectoption> options = new list<selectoption>();
                      options.add(new selectoption('none','none'));
                      options.add(new selectoption('prospect','prospect'));
                      options.add(new selectoption('custemer-direct','custemer-direct'));
                      options.add(new selectoption('custemer-channel','custemer-channel'));
                      options.add(new selectoption('channel,patner-reseller','channel,patner-reseller'));
                      return options;
                      } 
  
   public list<selectoption> getrtng(){
   
                      list<selectoption> options = new list<selectoption>();
                      options.add(new selectoption('none','none'));
                      options.add(new selectoption('hot','hot'));
                      options.add(new selectoption('warm','warm'));
                      options.add(new selectoption('cold','cold'));
                     
  return options;
  } 
  
   public list<selectoption> getowsh(){
   
                      list<selectoption> options = new list<selectoption>();
                      options.add(new selectoption('none','none'));
                      options.add(new selectoption('private','private'));
                      options.add(new selectoption('public','public'));
                      options.add(new selectoption('subsidiary','subsidiary'));
                     
  return options;
  } 
  public PageReference check(){
 
                      system.debug('..........'+rting); 
                      system.debug('..........'+typ); 
                      system.debug('..........'+wship); 
                      return null;
 } 
  

 public PageReference save_new(){
                       account__c ac = new account__c();
                       ac.account__c = acn;
                       ac.Account_Name__c = acname;
                       ac.Account_Number__c = acnmbr;
                       ac.Annual_Revenue__c = anlrev;
                       ac.phone__c = pne; 
                       ac.Email__c = emal;
                       ac.Billing_Stree__c = bstreet;
                       ac.Billing_City__c = bcty;
                       ac.Billing_State_Province__c= bstatp;
                       ac.Billing_Zip_Postal_Code__c = pcod; 
                       ac.Billing_Country__c = cuntry;   
                       ac.Shipping_Street__c = shstret; 
                       ac.Shipping_City__c = shcty; 
                       ac.Shipping_State_Province__c= shstatp;
                       ac.Shipping_Country__c = shcuntry;
                       ac.Rating__c = rting;
                       ac.Type__c = typ;
                       ac.Ownership__c = wship; 
                       insert ac;
                      PageReference pr=new pageReference('https://c.ap1.visual.force.com/apex/account');
                                return pr;
   }
                        public PageReference cancel(){
                        return null;
   }
   
   
                        account__c ac1;
                       public account__c getproxyObject2() { 
                          ac1 = new account__c();
                          return ac1;
       }
   
   
   
   
   
   
}