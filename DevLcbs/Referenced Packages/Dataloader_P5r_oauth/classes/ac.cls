public with sharing class ac {

     public String account { get; set; }

     public String datepickers { get; set; }

     public String datename { get; set; }

     public String con { get; set; }
    
     public String d_date_str { get; set; }

     public String nwsd { get; set; }


     public Date getEnddt() {
        return null;
    }
     
     public Date strtdt { get; set; }

     public String des { get; set; }

     public Integer numse { get; set; }
     
     public Decimal expres { get; set; }
     
     public Integer actcost { get; set; }
     
     public Integer bugcost { get; set; }
     
     public Integer exprevenue { get; set; }
     
     public String My_Campaign { get; set; }
     
     public String sta { get; set; }
     
     public String typ { get; set; }
     
     public String campname { get; set; }
     
     public boolean chk { get; set; }


     public List<SelectOption> getStas() {
     
      List<SelectOption> options  = new List<SelectOption>();
     
      options.add(new SelectOption('None','--None--'));
      
      options.add(new SelectOption('Planned','Planned'));
      
      options.add(new SelectOption('In Progress','In Progress'));
      
      options.add(new SelectOption('Completed','Completed'));
      
      options.add(new SelectOption('Aborted','Aborted')); 
      
        return options;
    }


    public  List<SelectOption> getTyps() {
    
      List<SelectOption> options  = new List<SelectOption>();
      
      options.add(new SelectOption('None','--None--'));
      
      options.add(new SelectOption('Conference','Conference'));
      
      options.add(new SelectOption('Webinar','Webinar'));
      
      options.add(new SelectOption('Trade Show','Trade Show'));
      
      options.add(new SelectOption('Public Relations','Public Relations'));
      
      options.add(new SelectOption('Partners','Partners'));
      
      options.add(new SelectOption('Referral Programs','Referral Programs'));
      
      options.add(new SelectOption('Advertisement','Advertisement'));
      
      options.add(new SelectOption('Banner Ads','Banner Ads'));
      
      options.add(new SelectOption('Direct Mail','Direct Mail'));
      
      options.add(new SelectOption('Email','Email'));
      
      options.add(new SelectOption('Telemarketing','Telemarketing'));
      
      options.add(new SelectOption('Other','Other'));
      
      return options;
    }
    
     public ac() {
     
      chk = False;
      
    }
          
    public PageReference cancelrecord() {
    
     return null;
     
    }
    
    
    public PageReference saveandnew() {
    
      return null;
      
    }
  
    public PageReference check() {
    
     system.debug('____________'+typ);
     
     system.debug('____________'+sta );
     
        return null;
    }
      
      
}