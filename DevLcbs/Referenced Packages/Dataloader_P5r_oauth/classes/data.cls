public with sharing class data {

          public List<account> ac;
          
          public integer index {set;get;}
          
          public integer list_size {set;get;}
          
          public integer total_size;
          
          public list<account> getac(){
           
           ac = [select name,AccountNumber,Site from account limit 5 offset:index];
           
           return ac;
            }
            
          public data(){
          
          total_size = [select count() from account];
          
            }
          public integer gettotal_size(){
          
            return total_size;
            } 
            
         public integer gettotalpageges(){
         
             if(math.mod(total_size,list_size)>0){
    
             return total_size/list_size+1;
           }
   
    else{
    
    return total_size/list_size;
    
    }

    }    
        
    public PageReference frst() {
    
    index=0;
    
    
        return null;
    }
    
    
    public PageReference second() {
     index= index -list_size;
    
        return null;
    }
    
    
    public PageReference third() {
    
    index = index+list_size;
 
    
        return null;
    }

    public PageReference fourth() {
       // index = total_size-list_size;
       
         index = total_size-math.mod(total_size,list_size);
            return null;
    }
      
      public boolean getPrevious() {
      
      if(index>0)     
      return false;
     
    else return true;
             
    }  
    
     public boolean getNext() {
     if(index+list_size < total_size){
      
      return false;
      }
    else{
    return true;
    }     
   }
  public Integer getPageNumber() {
  
   return index/list_size + 1;

     // return null;

   }
 
 //detail pages

    public String store{set;get;} 
    
    public PageReference redirect() {
    

     return null;
    }

    list<account> a = new list<account>();
    public list<account> getRecords() {
    a = [select name,id from account];
        return a;
    }      
}