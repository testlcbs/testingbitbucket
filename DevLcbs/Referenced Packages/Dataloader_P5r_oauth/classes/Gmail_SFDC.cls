global class Gmail_SFDC implements Messaging.InboundEmailHandler{

        global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail me, Messaging.InboundEnvelope env){
        
        Messaging.InboundEmailResult er = new Messaging.InboundEmailResult();
        
        inboundemail__c eo = new inboundemail__c();
        
        try{
        
        eo=[select name,Body__c,Email__c,Subject__c from inboundemail__c where name =:me.fromName limit 1];
        
        }
        catch(Exception e){}
        
        /*if(eo.id!= null){
            
            eo.kr__Subject__c = me.subject;
            eo.kr__Body__c = me.plainTextBody;
                    
        }
        else{
            
            eo.name = me.fromName;
            eo.kr__Subject__c = me.subject;
            eo.kr__Email_addr__c = me.fromAddress;
            eo.kr__Body__c = me.plainTextBody;
        
        }
        upsert eo;*/
        if(eo.id== null){
            eo.name = me.fromName;
            eo.Subject__c = me.subject;
            eo.Email__c = me.fromAddress;
            eo.Body__c = me.plainTextBody;
            insert eo;
            if(me.binaryAttachments!=null&&me.binaryAttachments.size()>0)
               {
               for(integer i=0;i<me.binaryAttachments.size();i++)
               {
                     Attachment att = new Attachment();
                   att.parentId = eo.id;
                   att.Name = me.binaryAttachments[i].filename;
                   att.Body = me.binaryAttachments[i].body;
                   insert att;
               }
               } 
               
              if(me.textAttachments!=null && me.textAttachments.size()>0)
              {
              for(integer i=0;i<me.textAttachments.size();i++)
              {
                 Attachment att = new Attachment();
                   att.parentId = eo.id;
                   att.Name = me.binaryAttachments[i].filename;
                   att.Body = me.binaryAttachments[i].body;
                   insert att;  
              }
              }
              
         
        }
        else{
            
            eo.name = me.fromName;
            eo.Subject__c = me.subject;
            eo.Body__c = me.plainTextBody;
            update eo;
           }
            
        
            
        return er;
    }

}