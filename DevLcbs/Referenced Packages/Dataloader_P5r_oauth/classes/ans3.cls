public with sharing class ans3 {
   public string ans10{set;get;}
   public string ans11{set;get;}
   public string ans12{set;get;}
   public string ans13{set;get;}
   public string ans14{set;get;}
   public string ans15{set;get;}
   public string ans16{set;get;}
   public string ans17{set;get;}
   public string ans18{set;get;}
   public string ans19{set;get;}
   
   public void ans10(){
   ans10 = ' Apex Classes ';
   }
    public void ans11(){
   ans11 = 'Radio Button';
   }
   
   public void ans12(){
   ans12 = 'Checkbox';
   }
  
   public void ans13(){
   ans13 = 'Yes';
   }
   
    public void ans14(){
   ans14 = 'Create an Event';
   }
    public void ans15(){
   ans15 = 'Tags can be enabled by enabling Tags permission for the Profiles';
   }
    public void ans16(){
   ans16 = 'True';
   }
    public void ans17(){
   ans17 = 'Report Name';
   }
    public void ans18(){
   ans18 = 'Master - Detail Relationship';
   }
    public void ans19(){
   ans19 = 'Delete the Record';
   }
   
}