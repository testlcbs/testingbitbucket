@RestResource(urlMapping='/account/*')
global with sharing class sampleRest
{

    @HttpDelete
    global static void doDelete()
    {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String memberId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        account memb = [SELECT Id FROM account WHERE Id = :memberId];
        delete memb;
    }
 
    @HttpGet
    global static account doGet()
    {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String memberId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        account result = [SELECT Id, Name FROM account WHERE Id = :memberId];
        return result;
    }
 
  @HttpPost
    global static String doPost(String name)
    {
        account m = new account();
        m.Name = name;
        insert m;
        return m.Id;
    }
}