<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-Workspace</tab>
    <tab>standard-ContentSearch</tab>
    <tab>standard-ContentSubscriptions</tab>
    <tab>standard-ContentFavorites</tab>
    <tab>inboundemail__c</tab>
    <tab>hotel__c</tab>
    <tab>book__c</tab>
    <tab>contactc__c</tab>
    <tab>Survey_Customers_webtab</tab>
    <tab>Customer_Satisfaction_Survey__c</tab>
    <tab>Reassign_Accounts</tab>
</CustomApplication>
