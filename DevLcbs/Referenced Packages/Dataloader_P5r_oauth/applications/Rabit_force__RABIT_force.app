<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Rabit_force__RABIT_force</defaultLandingTab>
    <description>AutoRABIT stands for Rapid Automated Build Install Test Framework which is designed to be a powerful continuous delivery system for force.com application development.</description>
    <label>AutoRABIT</label>
    <logo>Rabit_force__rabit_img/Rabit_force__AutoRABIT_Logo.png</logo>
    <tab>Rabit_force__RABIT_force</tab>
</CustomApplication>
