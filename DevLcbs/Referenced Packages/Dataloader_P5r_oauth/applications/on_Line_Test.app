<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>on Line Test</label>
    <tab>standard-Chatter</tab>
    <tab>standard-File</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>user__c</tab>
    <tab>preparation</tab>
    <tab>emp1s</tab>
    <tab>job__c</tab>
    <tab>exp</tab>
    <tab>questionbank__c</tab>
    <tab>lead__c</tab>
    <tab>account__c</tab>
    <tab>campaign__c</tab>
    <tab>myaccount</tab>
    <tab>mylead</tab>
    <tab>mycampaigns</tab>
    <tab>myopportunity</tab>
    <tab>Top_X_designation__c</tab>
    <tab>course__c</tab>
    <tab>student__c</tab>
    <tab>inboundemail__c</tab>
    <tab>hotel__c</tab>
    <tab>book__c</tab>
    <tab>contactc__c</tab>
    <tab>Survey_Customers_webtab</tab>
    <tab>Customer_Satisfaction_Survey__c</tab>
    <tab>Reassign_Accounts</tab>
</CustomApplication>
